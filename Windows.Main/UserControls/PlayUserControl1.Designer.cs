﻿namespace Windows.Main.UserControls
{
    partial class PlayUserContro_1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel23 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel24 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel25 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel26 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel27 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel28 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel29 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel30 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel31 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel32 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel33 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel1 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel2 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel3 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel4 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel5 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel6 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel7 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel8 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel9 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel10 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            DevExpress.XtraEditors.Repository.TrackBarLabel trackBarLabel11 = new DevExpress.XtraEditors.Repository.TrackBarLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.progressPanel_translate = new DevExpress.XtraWaitForm.ProgressPanel();
            this.simpleButton_convert = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox_language = new System.Windows.Forms.GroupBox();
            this.comboBoxEdit_voiceLanguage = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox_voice = new System.Windows.Forms.GroupBox();
            this.comboBox_voice = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox_languageTo = new System.Windows.Forms.GroupBox();
            this.comboBoxEdit_destination = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox_volume = new System.Windows.Forms.GroupBox();
            this.trackBarControl_volume = new DevExpress.XtraEditors.TrackBarControl();
            this.groupBox_languageFrom = new System.Windows.Forms.GroupBox();
            this.comboBoxEdit_source = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox_rate = new System.Windows.Forms.GroupBox();
            this.trackBarControl_rate = new DevExpress.XtraEditors.TrackBarControl();
            this.simpleButton_translateText = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_library = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_resume = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_stop = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_pause = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_play = new DevExpress.XtraEditors.SimpleButton();
            this.windowsUIButtonPanel1.SuspendLayout();
            this.groupBox_language.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_voiceLanguage.Properties)).BeginInit();
            this.groupBox_voice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox_voice.Properties)).BeginInit();
            this.groupBox_languageTo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_destination.Properties)).BeginInit();
            this.groupBox_volume.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_volume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_volume.Properties)).BeginInit();
            this.groupBox_languageFrom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_source.Properties)).BeginInit();
            this.groupBox_rate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_rate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_rate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.BackgroundImage = global::Windows.Main.Properties.Resources.audio_speakers_red_background_18815930;
            this.windowsUIButtonPanel1.Controls.Add(this.progressPanel_translate);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_convert);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_language);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_voice);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_languageTo);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_volume);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_languageFrom);
            this.windowsUIButtonPanel1.Controls.Add(this.groupBox_rate);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_translateText);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_library);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_resume);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_stop);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_pause);
            this.windowsUIButtonPanel1.Controls.Add(this.simpleButton_play);
            this.windowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(0, 0);
            this.windowsUIButtonPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1670, 187);
            this.windowsUIButtonPanel1.TabIndex = 1;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Click += new System.EventHandler(this.windowsUIButtonPanel1_Click);
            // 
            // progressPanel_translate
            // 
            this.progressPanel_translate.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressPanel_translate.Appearance.Options.UseBackColor = true;
            this.progressPanel_translate.Description = "Translating ...";
            this.progressPanel_translate.Location = new System.Drawing.Point(1438, 25);
            this.progressPanel_translate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressPanel_translate.Name = "progressPanel_translate";
            this.progressPanel_translate.Size = new System.Drawing.Size(153, 36);
            this.progressPanel_translate.TabIndex = 11;
            this.progressPanel_translate.Text = "Tranlate";
            // 
            // simpleButton_convert
            // 
            this.simpleButton_convert.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_convert.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.simpleButton_convert.Appearance.Options.UseFont = true;
            this.simpleButton_convert.Appearance.Options.UseForeColor = true;
            this.simpleButton_convert.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_convert.Location = new System.Drawing.Point(874, 87);
            this.simpleButton_convert.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_convert.Name = "simpleButton_convert";
            this.simpleButton_convert.Size = new System.Drawing.Size(115, 63);
            this.simpleButton_convert.TabIndex = 5;
            this.simpleButton_convert.Text = "Save Audio";
            this.simpleButton_convert.Click += new System.EventHandler(this.simpleButton_convert_Click);
            // 
            // groupBox_language
            // 
            this.groupBox_language.BackgroundImage = global::Windows.Main.Properties.Resources.red_audio_speakers_music_background_FYMH14;
            this.groupBox_language.Controls.Add(this.comboBoxEdit_voiceLanguage);
            this.groupBox_language.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_language.ForeColor = System.Drawing.Color.White;
            this.groupBox_language.Location = new System.Drawing.Point(4, 91);
            this.groupBox_language.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_language.Name = "groupBox_language";
            this.groupBox_language.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_language.Size = new System.Drawing.Size(189, 78);
            this.groupBox_language.TabIndex = 5;
            this.groupBox_language.TabStop = false;
            this.groupBox_language.Text = "Set voice language";
            // 
            // comboBoxEdit_voiceLanguage
            // 
            this.comboBoxEdit_voiceLanguage.EditValue = "language";
            this.comboBoxEdit_voiceLanguage.Location = new System.Drawing.Point(23, 32);
            this.comboBoxEdit_voiceLanguage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxEdit_voiceLanguage.Name = "comboBoxEdit_voiceLanguage";
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.BackColor = System.Drawing.Color.Tomato;
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.Options.UseBackColor = true;
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit_voiceLanguage.Properties.Appearance.Options.UseForeColor = true;
            this.comboBoxEdit_voiceLanguage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_voiceLanguage.Properties.DropDownRows = 4;
            this.comboBoxEdit_voiceLanguage.Properties.Items.AddRange(new object[] {
            "English",
            "French"});
            this.comboBoxEdit_voiceLanguage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit_voiceLanguage.Size = new System.Drawing.Size(117, 30);
            this.comboBoxEdit_voiceLanguage.TabIndex = 4;
            this.comboBoxEdit_voiceLanguage.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit_voiceLanguage_SelectedIndexChanged);
            // 
            // groupBox_voice
            // 
            this.groupBox_voice.BackgroundImage = global::Windows.Main.Properties.Resources.red_audio_speakers_music_background_FYMH14;
            this.groupBox_voice.Controls.Add(this.comboBox_voice);
            this.groupBox_voice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_voice.ForeColor = System.Drawing.Color.White;
            this.groupBox_voice.Location = new System.Drawing.Point(4, 16);
            this.groupBox_voice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_voice.Name = "groupBox_voice";
            this.groupBox_voice.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_voice.Size = new System.Drawing.Size(189, 68);
            this.groupBox_voice.TabIndex = 1;
            this.groupBox_voice.TabStop = false;
            this.groupBox_voice.Text = "Voice gender";
            // 
            // comboBox_voice
            // 
            this.comboBox_voice.EditValue = "Not Set";
            this.comboBox_voice.Location = new System.Drawing.Point(23, 30);
            this.comboBox_voice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_voice.Name = "comboBox_voice";
            this.comboBox_voice.Properties.Appearance.BackColor = System.Drawing.Color.Tomato;
            this.comboBox_voice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_voice.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.comboBox_voice.Properties.Appearance.Options.UseBackColor = true;
            this.comboBox_voice.Properties.Appearance.Options.UseFont = true;
            this.comboBox_voice.Properties.Appearance.Options.UseForeColor = true;
            this.comboBox_voice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBox_voice.Properties.DropDownRows = 3;
            this.comboBox_voice.Properties.Items.AddRange(new object[] {
            "Female",
            "Male"});
            this.comboBox_voice.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBox_voice.Size = new System.Drawing.Size(117, 30);
            this.comboBox_voice.TabIndex = 4;
            this.comboBox_voice.SelectedIndexChanged += new System.EventHandler(this.comboBox_voice_SelectedIndexChanged_1);
            // 
            // groupBox_languageTo
            // 
            this.groupBox_languageTo.BackgroundImage = global::Windows.Main.Properties.Resources.red_audio_speakers_music_background_FYMH14;
            this.groupBox_languageTo.Controls.Add(this.comboBoxEdit_destination);
            this.groupBox_languageTo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_languageTo.ForeColor = System.Drawing.Color.White;
            this.groupBox_languageTo.Location = new System.Drawing.Point(1393, 87);
            this.groupBox_languageTo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_languageTo.Name = "groupBox_languageTo";
            this.groupBox_languageTo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_languageTo.Size = new System.Drawing.Size(174, 81);
            this.groupBox_languageTo.TabIndex = 10;
            this.groupBox_languageTo.TabStop = false;
            this.groupBox_languageTo.Text = "To";
            // 
            // comboBoxEdit_destination
            // 
            this.comboBoxEdit_destination.EditValue = " Language";
            this.comboBoxEdit_destination.Location = new System.Drawing.Point(8, 32);
            this.comboBoxEdit_destination.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxEdit_destination.Name = "comboBoxEdit_destination";
            this.comboBoxEdit_destination.Properties.Appearance.BackColor = System.Drawing.Color.Tomato;
            this.comboBoxEdit_destination.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit_destination.Properties.Appearance.Options.UseBackColor = true;
            this.comboBoxEdit_destination.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit_destination.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_destination.Properties.Items.AddRange(new object[] {
            "English",
            "French"});
            this.comboBoxEdit_destination.Size = new System.Drawing.Size(161, 30);
            this.comboBoxEdit_destination.TabIndex = 0;
            this.comboBoxEdit_destination.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit_destination_SelectedIndexChanged);
            // 
            // groupBox_volume
            // 
            this.groupBox_volume.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.groupBox_volume.Controls.Add(this.trackBarControl_volume);
            this.groupBox_volume.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_volume.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox_volume.Location = new System.Drawing.Point(537, 74);
            this.groupBox_volume.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_volume.Name = "groupBox_volume";
            this.groupBox_volume.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_volume.Size = new System.Drawing.Size(318, 102);
            this.groupBox_volume.TabIndex = 2;
            this.groupBox_volume.TabStop = false;
            this.groupBox_volume.Text = "Volume";
            // 
            // trackBarControl_volume
            // 
            this.trackBarControl_volume.EditValue = 10;
            this.trackBarControl_volume.Location = new System.Drawing.Point(7, 21);
            this.trackBarControl_volume.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trackBarControl_volume.Name = "trackBarControl_volume";
            this.trackBarControl_volume.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackBarControl_volume.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            trackBarLabel23.Label = "0";
            trackBarLabel24.Label = "1";
            trackBarLabel24.Value = 1;
            trackBarLabel25.Label = "2";
            trackBarLabel25.Value = 2;
            trackBarLabel26.Label = "3";
            trackBarLabel26.Value = 3;
            trackBarLabel27.Label = "4";
            trackBarLabel27.Value = 4;
            trackBarLabel28.Label = "5";
            trackBarLabel28.Value = 5;
            trackBarLabel29.Label = "6";
            trackBarLabel29.Value = 6;
            trackBarLabel30.Label = "7";
            trackBarLabel30.Value = 7;
            trackBarLabel31.Label = "8";
            trackBarLabel31.Value = 8;
            trackBarLabel32.Label = "9";
            trackBarLabel32.Value = 9;
            trackBarLabel33.Label = "10";
            trackBarLabel33.Value = 10;
            this.trackBarControl_volume.Properties.Labels.AddRange(new DevExpress.XtraEditors.Repository.TrackBarLabel[] {
            trackBarLabel23,
            trackBarLabel24,
            trackBarLabel25,
            trackBarLabel26,
            trackBarLabel27,
            trackBarLabel28,
            trackBarLabel29,
            trackBarLabel30,
            trackBarLabel31,
            trackBarLabel32,
            trackBarLabel33});
            this.trackBarControl_volume.Properties.ShowLabels = true;
            this.trackBarControl_volume.Size = new System.Drawing.Size(293, 86);
            this.trackBarControl_volume.TabIndex = 0;
            this.trackBarControl_volume.Value = 10;
            this.trackBarControl_volume.EditValueChanged += new System.EventHandler(this.trackBarControl_volume_EditValueChanged_1);
            // 
            // groupBox_languageFrom
            // 
            this.groupBox_languageFrom.BackgroundImage = global::Windows.Main.Properties.Resources.red_audio_speakers_music_background_FYMH14;
            this.groupBox_languageFrom.Controls.Add(this.comboBoxEdit_source);
            this.groupBox_languageFrom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_languageFrom.ForeColor = System.Drawing.Color.White;
            this.groupBox_languageFrom.Location = new System.Drawing.Point(1224, 87);
            this.groupBox_languageFrom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_languageFrom.Name = "groupBox_languageFrom";
            this.groupBox_languageFrom.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_languageFrom.Size = new System.Drawing.Size(162, 81);
            this.groupBox_languageFrom.TabIndex = 9;
            this.groupBox_languageFrom.TabStop = false;
            this.groupBox_languageFrom.Text = "From";
            // 
            // comboBoxEdit_source
            // 
            this.comboBoxEdit_source.EditValue = "Language";
            this.comboBoxEdit_source.Location = new System.Drawing.Point(7, 32);
            this.comboBoxEdit_source.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxEdit_source.Name = "comboBoxEdit_source";
            this.comboBoxEdit_source.Properties.Appearance.BackColor = System.Drawing.Color.Tomato;
            this.comboBoxEdit_source.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit_source.Properties.Appearance.Options.UseBackColor = true;
            this.comboBoxEdit_source.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit_source.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_source.Properties.Items.AddRange(new object[] {
            "English",
            "French"});
            this.comboBoxEdit_source.Size = new System.Drawing.Size(149, 30);
            this.comboBoxEdit_source.TabIndex = 0;
            this.comboBoxEdit_source.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit_source_SelectedIndexChanged);
            // 
            // groupBox_rate
            // 
            this.groupBox_rate.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.groupBox_rate.Controls.Add(this.trackBarControl_rate);
            this.groupBox_rate.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_rate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox_rate.Location = new System.Drawing.Point(230, 66);
            this.groupBox_rate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_rate.Name = "groupBox_rate";
            this.groupBox_rate.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox_rate.Size = new System.Drawing.Size(257, 102);
            this.groupBox_rate.TabIndex = 3;
            this.groupBox_rate.TabStop = false;
            this.groupBox_rate.Text = "Rate";
            // 
            // trackBarControl_rate
            // 
            this.trackBarControl_rate.EditValue = 1;
            this.trackBarControl_rate.Location = new System.Drawing.Point(7, 25);
            this.trackBarControl_rate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trackBarControl_rate.Name = "trackBarControl_rate";
            this.trackBarControl_rate.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackBarControl_rate.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            trackBarLabel1.Label = "0";
            trackBarLabel2.Label = "1";
            trackBarLabel2.Value = 1;
            trackBarLabel3.Label = "2";
            trackBarLabel3.Value = 2;
            trackBarLabel4.Label = "3";
            trackBarLabel4.Value = 3;
            trackBarLabel5.Label = "4";
            trackBarLabel5.Value = 4;
            trackBarLabel6.Label = "5";
            trackBarLabel6.Value = 5;
            trackBarLabel7.Label = "6";
            trackBarLabel7.Value = 6;
            trackBarLabel8.Label = "7";
            trackBarLabel8.Value = 7;
            trackBarLabel9.Label = "8";
            trackBarLabel9.Value = 8;
            trackBarLabel10.Label = "9";
            trackBarLabel10.Value = 9;
            trackBarLabel11.Label = "10";
            trackBarLabel11.Value = 10;
            this.trackBarControl_rate.Properties.Labels.AddRange(new DevExpress.XtraEditors.Repository.TrackBarLabel[] {
            trackBarLabel1,
            trackBarLabel2,
            trackBarLabel3,
            trackBarLabel4,
            trackBarLabel5,
            trackBarLabel6,
            trackBarLabel7,
            trackBarLabel8,
            trackBarLabel9,
            trackBarLabel10,
            trackBarLabel11});
            this.trackBarControl_rate.Properties.ShowLabels = true;
            this.trackBarControl_rate.Size = new System.Drawing.Size(243, 86);
            this.trackBarControl_rate.TabIndex = 0;
            this.trackBarControl_rate.Value = 1;
            this.trackBarControl_rate.EditValueChanged += new System.EventHandler(this.trackBarControl_rate_EditValueChanged_2);
            // 
            // simpleButton_translateText
            // 
            this.simpleButton_translateText.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_translateText.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.simpleButton_translateText.Appearance.Options.UseFont = true;
            this.simpleButton_translateText.Appearance.Options.UseForeColor = true;
            this.simpleButton_translateText.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_translateText.Location = new System.Drawing.Point(1245, 10);
            this.simpleButton_translateText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_translateText.Name = "simpleButton_translateText";
            this.simpleButton_translateText.Size = new System.Drawing.Size(170, 50);
            this.simpleButton_translateText.TabIndex = 8;
            this.simpleButton_translateText.Text = "Translate document";
            this.simpleButton_translateText.Click += new System.EventHandler(this.simpleButton_translateText_Click);
            // 
            // simpleButton_library
            // 
            this.simpleButton_library.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_library.Appearance.Options.UseFont = true;
            this.simpleButton_library.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_library.Location = new System.Drawing.Point(279, 16);
            this.simpleButton_library.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_library.Name = "simpleButton_library";
            this.simpleButton_library.Size = new System.Drawing.Size(146, 38);
            this.simpleButton_library.TabIndex = 6;
            this.simpleButton_library.Text = "Back to library";
            this.simpleButton_library.Click += new System.EventHandler(this.simpleButton_library_Click);
            // 
            // simpleButton_resume
            // 
            this.simpleButton_resume.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_resume.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.simpleButton_resume.Appearance.Options.UseFont = true;
            this.simpleButton_resume.Appearance.Options.UseForeColor = true;
            this.simpleButton_resume.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_resume.Location = new System.Drawing.Point(699, 16);
            this.simpleButton_resume.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_resume.Name = "simpleButton_resume";
            this.simpleButton_resume.Size = new System.Drawing.Size(83, 38);
            this.simpleButton_resume.TabIndex = 3;
            this.simpleButton_resume.Text = "Resume";
            this.simpleButton_resume.Click += new System.EventHandler(this.simpleButton_resume_Click);
            // 
            // simpleButton_stop
            // 
            this.simpleButton_stop.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_stop.Appearance.ForeColor = System.Drawing.Color.Red;
            this.simpleButton_stop.Appearance.Options.UseFont = true;
            this.simpleButton_stop.Appearance.Options.UseForeColor = true;
            this.simpleButton_stop.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_stop.Location = new System.Drawing.Point(789, 16);
            this.simpleButton_stop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_stop.Name = "simpleButton_stop";
            this.simpleButton_stop.Size = new System.Drawing.Size(66, 38);
            this.simpleButton_stop.TabIndex = 2;
            this.simpleButton_stop.Text = "Stop";
            this.simpleButton_stop.Click += new System.EventHandler(this.simpleButton_stop_Click);
            // 
            // simpleButton_pause
            // 
            this.simpleButton_pause.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_pause.Appearance.ForeColor = System.Drawing.Color.Olive;
            this.simpleButton_pause.Appearance.Options.UseFont = true;
            this.simpleButton_pause.Appearance.Options.UseForeColor = true;
            this.simpleButton_pause.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_pause.Location = new System.Drawing.Point(625, 16);
            this.simpleButton_pause.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_pause.Name = "simpleButton_pause";
            this.simpleButton_pause.Size = new System.Drawing.Size(66, 38);
            this.simpleButton_pause.TabIndex = 1;
            this.simpleButton_pause.Text = "Pause";
            this.simpleButton_pause.Click += new System.EventHandler(this.simpleButton_pause_Click);
            // 
            // simpleButton_play
            // 
            this.simpleButton_play.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton_play.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.simpleButton_play.Appearance.Options.UseFont = true;
            this.simpleButton_play.Appearance.Options.UseForeColor = true;
            this.simpleButton_play.BackgroundImage = global::Windows.Main.Properties.Resources._1;
            this.simpleButton_play.Location = new System.Drawing.Point(537, 16);
            this.simpleButton_play.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton_play.Name = "simpleButton_play";
            this.simpleButton_play.Size = new System.Drawing.Size(66, 38);
            this.simpleButton_play.TabIndex = 0;
            this.simpleButton_play.Text = "Play";
            this.simpleButton_play.Click += new System.EventHandler(this.simpleButton_play_Click);
            // 
            // PlayUserContro_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.windowsUIButtonPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PlayUserContro_1";
            this.Size = new System.Drawing.Size(1670, 187);
            this.windowsUIButtonPanel1.ResumeLayout(false);
            this.groupBox_language.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_voiceLanguage.Properties)).EndInit();
            this.groupBox_voice.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBox_voice.Properties)).EndInit();
            this.groupBox_languageTo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_destination.Properties)).EndInit();
            this.groupBox_volume.ResumeLayout(false);
            this.groupBox_volume.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_volume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_volume)).EndInit();
            this.groupBox_languageFrom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_source.Properties)).EndInit();
            this.groupBox_rate.ResumeLayout(false);
            this.groupBox_rate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_rate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl_rate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton_play;
        private DevExpress.XtraEditors.SimpleButton simpleButton_pause;
        private DevExpress.XtraEditors.SimpleButton simpleButton_stop;
        private DevExpress.XtraEditors.SimpleButton simpleButton_resume;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private System.Windows.Forms.GroupBox groupBox_voice;
        private System.Windows.Forms.GroupBox groupBox_volume;
        private System.Windows.Forms.GroupBox groupBox_rate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBox_voice;
        private DevExpress.XtraEditors.TrackBarControl trackBarControl_volume;
        private DevExpress.XtraEditors.TrackBarControl trackBarControl_rate;
        private DevExpress.XtraEditors.SimpleButton simpleButton_library;
        private DevExpress.XtraEditors.SimpleButton simpleButton_translateText;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox_languageTo;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit_destination;
        private System.Windows.Forms.GroupBox groupBox_languageFrom;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit_source;
        private System.Windows.Forms.GroupBox groupBox_language;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit_voiceLanguage;
        private DevExpress.XtraEditors.SimpleButton simpleButton_convert;
        private DevExpress.XtraWaitForm.ProgressPanel progressPanel_translate;
    }
}
